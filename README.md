# Kort introduksjon til C++

I faget brukes C++ for å demonstrere noen konsepter. Dette dokumentet gir deg en kort introduksjon
til programmeringsspråket, nok til at du kan følge undervisningen.

## Utviklingsmiljø

Om du ønsker å teste ut eksemplene gitt i denne introduksjonen anbefales det å bruke følgende:

- Linux eller MacOS
  - Anbefalt Linux distribusjon er [Manjaro](https://manjaro.org/) (XFCE Desktop) som har de nyeste
    programvarebibliotekene. Kan installeres i for eksempel
    [VirtualBox](https://www.virtualbox.org/).
- IDE'en [juCi++](https://gitlab.com/cppit/jucipp), en resurseffektiv og enkel C/C++ IDE som støtter
  de nyeste C++ standardene, og anvender de nyeste programvarebibliotekene og verktøyene
  - Andre IDE'er som for eksempel Visual Studio Code (du må finne addons selv) og CLion kan også
    brukes

Om du installerte juCi++, skriv `juci` i en terminal, og velg New Project og C++ under File-menyen.
Kjør eksemplene ved å lime inn kodesnuttene i main.cpp og deretter velge Compile and Run i
Project-menyen.

## Et enkelt program

Vi starter enklest mulig:

```c++
#include <iostream>

using namespace std; // Without this line one would need to write std::cout and std::endl below

int main() {
  cout << "Hello World" << endl;
}
// Output:
// Hello World
```

Derimot er her allerede nå noe nytt i forhold til hva du er vandt til fra tidligere. I stedet for å
bruke en _print_-funksjon til å skrive ut tekst til en terminal, brukes _strømmer_ i stedet for.
`cout` er en slik _strøm_ som er knyttet mot _standard output_, og når vi skriver data, ved hjelp av
operatoren `<<`, til denne _strømmen_ blir data'en vist i terminalen. Skrives `endl` til _strømmen_,
sender vi en newline (`\n` eller `\r\n` alt etter hvilke OS du sitter på), og i tillegg blir
innholdet i strømmen, som kan være midlertidlig lagret, tvunget ut til terminalen.

Strømmer generaliserer måten en jobber med utskrift og input på. La oss si at vi vil skrive til en
fil i stedet for:

```c++
#include <fstream>

using namespace std;

int main() {
  fstream file("output.txt", ios::out);
  file << "Hello World" << endl;
}
// Output in output.txt file:
// Hello World
```

Poenget er at koden er svært lik, vi skriver bare til en _filstrøm_ i stedet for å skrive til
_standard output_.

## Levetid

C++ har ikke automatisert _garbage collection_ siden dette strider mot prinsippet til C++ om ingen
overhead ved abstraksjon. Derimot blir variabler som blir opprettet i et _scope_, frigjort ved
slutten av _scopet_:

```c++
#include <iostream>

using namespace std;

class A {
public:
  A() {
    cout << "Constructed" << endl;
  }
  ~A() { // Special function that is called when an instance of A is going to be destructed
    cout << "Destructed" << endl;
  }
};

int main() {
  {      // Start of scope
    A a; // a is constructed
  }      // End of scope, a is destructed
}

// Output:
// Constructed
// Destructed
```

Legg merke til at destruksjonen av variabler er deterministisk. Dette kan utnyttes i ulike
sammenhenger. Interesserte kan lese mer om dette temaet
[her](https://en.wikipedia.org/wiki/Resource_acquisition_is_initialization).

## Referanser

Siden C++ ikke har automatisert _garbage collection_ så må en være ekstra på vakt når en for
eksempel skal låne ut en variabel til en funksjon. Studer eksempelet under som viser en enkel
`println`-funksjon:

```c++
#include <iostream>
#include <string>

using namespace std;

/// Outputs str, a newline, and flushes standard output.
void println(string str) {
  cout << str << endl;
}

int main() {
  string hello_str("Hello World");
  println(hello_str);
}
// Output:
// Hello World
```

Parametertypen til `println` er `string`, og siden C++ kopierer variabler hvis ikke noe annet er
oppgitt, blir det her opprettet et ekstra `string`-objekt i `println` funksjonen, altså en kopi av
`hello_str`-variabelen i `str`-variabelen. For å unngå dette, kan vi bruke referanser:

```c++
#include <iostream>
#include <string>

using namespace std;

/// Outputs str, a newline, and flushes standard output.
void println(string &str) {
  cout << str << endl;
}

int main() {
  string hello_str("Hello World");
  println(hello_str);
}
// Output:
// Hello World
```

Parametertypen til `println` er nå `string &`. I eksempelet over betyr det at C++ kompileren tolker
`str`-variabelen til å faktisk være `hello_str`-variabelen når `println` blir kalt. Det blir da ikke
opprettet noen kopi av `hello_str`.

Nå som `println` inneholder en referanse til `hello_str`, kan `println`-funksjonen også gjøre
endringer på `hello_str`. I de fleste tilfeller vil vi ikke det, og da bør vi markere referansen som
`const`:

```c++
#include <iostream>
#include <string>

using namespace std;

void println(const string &str) {
  cout << str << endl;
}

int main() {
  string hello_str("Hello World");
  println(hello_str);
}
// Output:
// Hello World
```

Det vil nå ikke være mulig å gjøre endringer på `str`, og dermed `hello_str`, inne i
`println`-funksjonen. Dette gjør at programmereren kan være trygg på at `hello_str` ikke endres når
`println` blir kalt.

## For-each løkker med referanser

En kan bruke referanser i for-each løkker på denne måten:

```c++
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  vector<string> words = {"Hello", "World"};

  for(const auto &word : words) {
    cout << word << " ";
  }
  cout << endl;
}
// Output:
// Hello World
```

Her lagrer vi ordene `Hello` og `World` i konteineren `vector`. `vector` er potensielt mer effektiv
enn for eksempel Java's `ArrayList`, men i eksempelet over fungerer `vector` i bunn og grunn som
`ArrayList`. I for-løkken blir `word`, for hver iterasjon, satt til å referere til et element i
vektoren `words`.

`auto` skriver vi for å slippe å skrive opp igjen `string`. Kompilatoren forstår at `word` er av
typen `string`.

## Anonyme funksjoner

Vi kan også iterere gjennom en konteiner ved hjelp funksjonen `for_each`:

```c++
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
  vector<string> words = {"Hello", "World"};

  auto &output = cout; // Set output to standard output stream

  for_each(words.begin(), words.end(), [&output](const string &word) {
    output << word << " ";
  });
  output << endl;
}
// Output:
// Hello World

```

`for_each`-funksjonen er en av de enkleste funksjonelle algoritmene som tar en anonym funksjon som
parameter. I de to første parameterne til `for_each` oppgir vi starten og slutten til konteineren vi
vil iterere igjennom. Den anonyme funksjonen finner du som tredje argument, og er av formen
`[]() {}`.

Spesielt for C++ er at en kan, ved hjelp av `[]`-symbolene, spesifisere hvilke variabler en skal
_fange_ i den anonyme funksjonen. For å demonstrere dette er det laget en `output`-variabel som
spesifiserer hvilken strøm som skal brukes. Denne _fanges_ som en referanse, og på den måten kan vi
også bruke denne strømmen inne i den anonyme funksjonen.

`for_each`-funksjonen kaller den anonyme funksjonen for hvert element i `words`-vektoren, og
parameteren `word` blir satt til å være en `const`-referanse til de ulike elementene.

### Hvordan representeres en anonym funksjon

La oss si at vi lager den anonyme funksjonen ved hjelp av en klasse i stedet for, og samtidig kan vi
se og forstå hvordan slike anonyme funksjoner kan bygges opp internt i en kompilator:

```c++
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class PrintWord {
public:
  ostream &output;

  // Constructor that initializes the output member variable
  PrintWord(ostream &output_) : output(output_) {}

  // Makes it possible to call an instance of PrintWord as a function, for instance:
  //   PrintWord print_word(cout);
  //   print_word("word");
  void operator()(const string &word) {
    output << word << " ";
  }
};

int main() {
  vector<string> words = {"Hello", "World"};

  auto &output = cout; // Set output to standard output stream

  PrintWord print_word(output);
  for_each(words.begin(), words.end(), print_word);
  output << endl;
}
// Output:
// Hello World
```

I kildekoden over, kaller `for_each` funksjonen `operator()(const string &word)` til
`print_word`-objektet der `word` blir satt til de ulike elementene i `words`-vektoren.
